package com.tw.parkingLot;

import com.tw.parkingLot.Exceptions.AllManagedParkingLotsFullException;
import com.tw.parkingLot.Exceptions.AlreadyParkedException;
import com.tw.parkingLot.Exceptions.ParkingLotFullException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ParkingLotAttendant implements ParkingLotObserver {
    List<ParkingLot> managedParkingLots = new ArrayList<>();
    HashMap<Parkable, ParkingLot> allocation = new HashMap<>();

    public void isResponsibleFor(ParkingLot managedParkingLot) {
        managedParkingLot.assignObserver(this);

        if (!managedParkingLot.isParkingLotFull()) {
            managedParkingLots.add(managedParkingLot);
        }
    }

    public void directs(Parkable parkableVehicle) throws AlreadyParkedException, ParkingLotFullException,
            AllManagedParkingLotsFullException {
        if (allocation.containsKey(parkableVehicle)) {
            throw new AlreadyParkedException();
        }

        if(managedParkingLots.isEmpty()){
            throw new AllManagedParkingLotsFullException();
        }
        ParkingLot parkingLotToBeParked = managedParkingLots.get(0);
        parkingLotToBeParked.park(parkableVehicle);
        allocation.put(parkableVehicle, parkingLotToBeParked);
    }

    @Override
    public void notifyWhenParkingLotIsFull(ParkingLot managedParkingLot) {
        managedParkingLots.remove(managedParkingLot);
    }

    @Override
    public void notifyWhenParkingLotHasSpace(ParkingLot managedParkingLot) {
        managedParkingLots.add(managedParkingLot);

    }
}
