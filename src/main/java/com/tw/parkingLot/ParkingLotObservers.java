package com.tw.parkingLot;

import java.util.ArrayList;

public class ParkingLotObservers extends ArrayList<ParkingLotObserver> {
    public void notifyWhenParkingLotIsFull(ParkingLot parkingLot) {
        for (ParkingLotObserver observer : this)
        {
            observer.notifyWhenParkingLotIsFull(parkingLot);
        }
    }

    public void notifyWhenParkingLotHasSpace(ParkingLot parkingLot) {
        for (ParkingLotObserver observer : this)
        {
            observer.notifyWhenParkingLotHasSpace(parkingLot);
        }
    }
}
