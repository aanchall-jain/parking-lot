package com.tw.parkingLot;

public interface ParkingLotObserver {
    void notifyWhenParkingLotIsFull(ParkingLot parkingLot);

    void notifyWhenParkingLotHasSpace(ParkingLot parkingLot);
}
