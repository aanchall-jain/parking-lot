package com.tw.parkingLot;

import com.tw.parkingLot.Exceptions.AlreadyParkedException;
import com.tw.parkingLot.Exceptions.NotParkedExcpetion;
import com.tw.parkingLot.Exceptions.ParkingLotFullException;

import java.util.HashSet;
import java.util.Set;

public class ParkingLot {
    private final int parkingLotSize;
    Set<Parkable> parkedVehicles = new HashSet<>();
    private final ParkingLotObservers parkingLotObservers =  new ParkingLotObservers();

    public ParkingLot(int parkingLotSize) {
        this.parkingLotSize = parkingLotSize;
    }

    public void park(Parkable parkableVehicle) throws AlreadyParkedException, ParkingLotFullException {
        if (parkedVehicles.contains(parkableVehicle)) {
            throw new AlreadyParkedException();
        }
        if (isParkingLotFull()) {
            throw new ParkingLotFullException();
        }
        parkedVehicles.add(parkableVehicle);
        if (isParkingLotFull()) {
            parkingLotObservers.notifyWhenParkingLotIsFull(this);
        }
    }

    boolean isParkingLotFull() {
        return parkingLotSize == parkedVehicles.size();
    }

    public void unPark(Parkable parkableVehicle) throws NotParkedExcpetion {
        if (!parkedVehicles.contains(parkableVehicle)) {
            throw new NotParkedExcpetion();
        }
        parkedVehicles.remove(parkableVehicle);
        if (ifParkingLotFullBeforeUnPark()) {
            parkingLotObservers.notifyWhenParkingLotHasSpace(this);
        }
    }

    private boolean ifParkingLotFullBeforeUnPark() {
        return (parkingLotSize) - 1 == parkedVehicles.size();
    }

    public void assignObserver(ParkingLotObserver parkingLotObserver) {
        this.parkingLotObservers.add(parkingLotObserver);
    }
}
