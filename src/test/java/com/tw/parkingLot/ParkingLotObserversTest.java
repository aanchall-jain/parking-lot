package com.tw.parkingLot;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class ParkingLotObserversTest {
    private ParkingLot parkingLotOne;
    private ParkingLot parkingLotTwo;
    private static ParkingLot parkingLot;
    private static Parkable carOne;
    private static Parkable carTwo;
    private static ParkingLotObserver parkingLotOwner;
    private static ParkingLotObserver trafficCop;

    @BeforeEach
    void beforeEach() {
        parkingLotOne = new ParkingLot(1);
        parkingLotTwo = new ParkingLot(2);
        parkingLotOwner = mock(ParkingLotObserver.class);
        trafficCop = mock(ParkingLotObserver.class);
    }

    @BeforeAll
    static void beforeAll() {
        carOne = mock(Parkable.class);
        carTwo = mock(Parkable.class);
    }

    @Test
    void toNotifyObserversWhenTheParkingLotIsFull() throws Exception {
        parkingLotOne.assignObserver(parkingLotOwner);
        parkingLotOne.assignObserver(trafficCop);

        parkingLotOne.park(carOne);

        verify(parkingLotOwner, times(1)).notifyWhenParkingLotIsFull(parkingLot);
        verify(trafficCop, times(1)).notifyWhenParkingLotIsFull(parkingLot);
    }

    @Test
    void notToNotifyObserversWhenTheParkingLotIsNotFull() throws Exception {
        parkingLotOne.assignObserver(parkingLotOwner);
        parkingLotTwo.assignObserver(trafficCop);

        parkingLotTwo.park(carOne);

        verify(parkingLotOwner, never()).notifyWhenParkingLotIsFull(parkingLot);
        verify(trafficCop, never()).notifyWhenParkingLotIsFull(parkingLot);
    }

    @Test
    void toNotifyObserversWheneverParkingLotHasFreeSpace() throws Exception {
        parkingLotOne.assignObserver(parkingLotOwner);
        parkingLotOne.assignObserver(trafficCop);

        parkingLotOne.park(carOne);
        parkingLotOne.unPark(carOne);

        verify(parkingLotOwner, times(1)).notifyWhenParkingLotHasSpace(parkingLot);
        verify(trafficCop, times(1)).notifyWhenParkingLotHasSpace(parkingLot);
    }

    @Test
    void toNotifyObserversWhenParkingLotHasFreeSpaceAgain() throws Exception {
        parkingLotTwo.assignObserver(parkingLotOwner);
        parkingLotTwo.assignObserver(trafficCop);

        parkingLotTwo.park(carOne);
        parkingLotTwo.park(carTwo);
        parkingLotTwo.unPark(carOne);

        verify(parkingLotOwner, times(1)).notifyWhenParkingLotHasSpace(parkingLot);
        verify(trafficCop, times(1)).notifyWhenParkingLotHasSpace(parkingLot);
    }

}
