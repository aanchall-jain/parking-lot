package com.tw.parkingLot;

import com.tw.parkingLot.Exceptions.AlreadyParkedException;
import com.tw.parkingLot.Exceptions.NotParkedExcpetion;
import com.tw.parkingLot.Exceptions.ParkingLotFullException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

public class ParkingLotTest {
    private ParkingLot parkingLotOne;
    private ParkingLot parkingLotTwo;
    private static ParkingLot parkingLot;
    private static Parkable carOne;
    private static Parkable carTwo;
    private static ParkingLotObserver parkingLotOwner;
    private static ParkingLotObserver trafficCop;

    @BeforeEach
    void beforeEach() {
        parkingLotOne = new ParkingLot(1);
        parkingLotTwo = new ParkingLot(2);
        parkingLotOwner = mock(ParkingLotObserver.class);
        trafficCop = mock(ParkingLotObserver.class);
    }

    @BeforeAll
    static void beforeAll() {
        carOne = mock(Parkable.class);
        carTwo = mock(Parkable.class);
    }

    @Test
    void toParkACarInTheParkingLot() {
        try {
            parkingLotOne.park(carOne);
        } catch (AlreadyParkedException exception) {
            fail("The car is already parked.");
        } catch (ParkingLotFullException exception) {
            fail("The parking lot is already full.");
        }
    }

    @Test
    void toThrowExceptionWhenTheCarIsAlreadyParked() throws Exception {
        parkingLotTwo.park(carOne);

        assertThrows(AlreadyParkedException.class, () -> parkingLotTwo.park(carOne));
    }

    @Test
    void toThrowExceptionWhenTheParkingLotIsFull() throws Exception {
        parkingLotOne.park(carOne);

        assertThrows(ParkingLotFullException.class, () -> parkingLotOne.park(carTwo));
    }

    @Test
    void toThrowExceptionWhenUnParkingACarWhichIsNotParked() throws Exception{
        parkingLotTwo.park(carOne);
        parkingLotTwo.park(carTwo);
        parkingLotTwo.unPark(carOne);

        assertThrows(NotParkedExcpetion.class, () -> parkingLotTwo.unPark(carOne));
    }

    @Test
    void toNotifyOwnerWhenTheParkingLotIsFull() throws Exception    {
        parkingLotOne.assignObserver(parkingLotOwner);
        parkingLotOne.park(carOne);

        verify(parkingLotOwner, times(1)).notifyWhenParkingLotIsFull(parkingLot);
    }

    @Test
    void notToNotifyOwnerWhenTheParkingLotIsNotFull() throws Exception    {
        parkingLotTwo.assignObserver(parkingLotOwner);
        parkingLotTwo.park(carOne);

        verify(parkingLotOwner, never()).notifyWhenParkingLotIsFull(parkingLot);
    }

    @Test
    void toNotifyTrafficCopWhenTheParkingLotIsFull() throws Exception    {
        parkingLotOne.assignObserver(trafficCop);
        parkingLotOne.park(carOne);

        verify(trafficCop, times(1)).notifyWhenParkingLotIsFull(parkingLot);
    }

    @Test
    void notToNotifyTrafficCopWhenTheParkingLotIsNotFull() throws Exception    {
        parkingLotTwo.assignObserver(trafficCop);
        parkingLotTwo.park(carOne);

        verify(trafficCop, never()).notifyWhenParkingLotIsFull(parkingLot);
    }

}
