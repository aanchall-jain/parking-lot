package com.tw.parkingLot;

import com.tw.parkingLot.Exceptions.AllManagedParkingLotsFullException;
import com.tw.parkingLot.Exceptions.AlreadyParkedException;
import com.tw.parkingLot.Exceptions.ParkingLotFullException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

public class ParkingLotAttendantTest {
    private ParkingLot parkingLotOne;
    private ParkingLot parkingLotTwo;
    private ParkingLot parkingLotThree;
    private ParkingLotAttendant parkingLotAttendant;
    private static Parkable carOne;
    private static Parkable carTwo;
    private static Parkable carThree;
    private static Parkable carFour;
    private static Parkable carFive;
    private static Parkable carSix;

    @BeforeEach
    void beforeEach() {
        parkingLotOne = new ParkingLot(1);
        parkingLotTwo = new ParkingLot(2);
        parkingLotThree = new ParkingLot(3);
        parkingLotAttendant = new ParkingLotAttendant();
    }

    @BeforeAll
    static void beforeAll() {
        carOne = mock(Parkable.class);
        carTwo = mock(Parkable.class);
        carThree = mock(Parkable.class);
        carFour = mock(Parkable.class);
        carFive = mock(Parkable.class);
        carSix = mock(Parkable.class);
    }

    @Test
    void toParkACarInTheOnlyManagedParkingLotWithSizeOne () throws Exception {
        parkingLotAttendant.isResponsibleFor(parkingLotOne);

        parkingLotAttendant.directs(carOne);

        assertThrows(ParkingLotFullException.class, () -> parkingLotOne.park(carTwo));
    }

    @Test
    void notToParkAnAlreadyParkedCarInTheOnlyManagedParkingLot () throws Exception
    {
        parkingLotAttendant.isResponsibleFor(parkingLotTwo);

        parkingLotAttendant.directs(carOne);

        assertThrows(AlreadyParkedException.class, () -> parkingLotTwo.park(carOne));
    }

    @Test
    void toParkACarInTheFirstManagedLot() throws Exception {
        parkingLotAttendant.isResponsibleFor(parkingLotOne);
        parkingLotAttendant.isResponsibleFor(parkingLotTwo);

        parkingLotAttendant.directs(carOne);

        assertThrows(ParkingLotFullException.class, () -> parkingLotOne.park(carTwo));
    }

    @Test
    void toParkACarInTheNextAvailableManagedLot() throws Exception {
        parkingLotAttendant.isResponsibleFor(parkingLotTwo);
        parkingLotAttendant.isResponsibleFor(parkingLotOne);

        parkingLotAttendant.directs(carOne);
        parkingLotAttendant.directs(carTwo);
        parkingLotAttendant.directs(carThree);

        assertThrows(ParkingLotFullException.class, () -> parkingLotOne.park(carFour));
    }

    @Test
    void toThrowExceptionWhenAllTheManagedParkingLotsAreFull() throws Exception {
        parkingLotAttendant.isResponsibleFor(parkingLotTwo);
        parkingLotAttendant.isResponsibleFor(parkingLotThree);

        parkingLotAttendant.directs(carOne);
        parkingLotAttendant.directs(carTwo);
        parkingLotAttendant.directs(carThree);
        parkingLotAttendant.directs(carFour);
        parkingLotAttendant.directs(carFive);

        assertThrows(AllManagedParkingLotsFullException.class, () -> parkingLotAttendant.directs(carSix));
    }
}
